package com.forgerock;

import java.util.*;

public class Filter {
	
	
	///// Properties /////
	private String initialExpression;
	private LinkedList<String> output = new LinkedList<String>();
	private enum LogicalOperator {
		// comparisons get first priority, then unary operators, then binary operators
		OR(1), AND(2), NOT(3), EXISTS(4), GT(5), LT(5), GE(5), LE(5), EQ(5);
		final int precedence;
		LogicalOperator(int l) { 
			precedence = l; 
		}
	}
	private static Map<String, LogicalOperator> LogicalOperators = new HashMap<String, LogicalOperator>(){{
		put("NOT", LogicalOperator.NOT);
		put("AND", LogicalOperator.AND);
		put("OR", LogicalOperator.OR);
		put("EXISTS", LogicalOperator.EXISTS);
		put(">", LogicalOperator.GT);
		put("<", LogicalOperator.LT);
		put(">=", LogicalOperator.GE);
		put("<=", LogicalOperator.LE);
		put("=", LogicalOperator.EQ);
	}};
	
	
	///// Constructors /////
	public Filter(String fullExpression) {
		
		// Save the expression given
		initialExpression = fullExpression;
		
		// Convert prefix to postfix
		createPostFixExpression(fullExpression);
		
		// display();
	}
	
	
	///// Public Methods /////
	public boolean matches(LinkedHashMap<String, String> user) {
		
		Deque<String> stack = new LinkedList<String>();
		String left, right;

		// Do postfix evaulation with output list
		for(int i = output.size() - 1; i >= 0; i--) {
			
			// System.out.println(stack);
			
			if (output.get(i).equals("EXISTS")) {
				right = stack.pop();
				
				// Check if property exists, push Boolean to stack
				stack.push(Boolean.toString(user.containsKey(right)));
			}
			else if (output.get(i).equals("NOT")) {
				right = stack.pop();
				
				// Invert then push back on stack
				stack.push(right.equals("true") ? "false" : "true");
			} 
			else if (output.get(i).equals("AND") || output.get(i).equals("OR")) {
				right = stack.pop();
				left = stack.pop();
				
				boolean rightBool = Boolean.valueOf(right);
				boolean leftBool = Boolean.valueOf(left);
				
				// Push result of AND or OR of both arguments back to the stack
				stack.push(String.valueOf(output.get(i).equals("AND") ? (leftBool && rightBool) : (leftBool || rightBool)).toString());
			}
			else if (output.get(i).equals(">")) {
				right = stack.pop();
				left = stack.pop();
				
				// Check if left is a key in user and if the right arg is an integer, do comparison, then push to stack
				if (user.containsKey(left) && isInteger(right)) {
					int num1 = Integer.parseInt(user.get(left));
					int num2 = Integer.parseInt(right);
					
					stack.push(Boolean.toString(num1 > num2));
				}
				else
					stack.push("false");
			}
			else if (output.get(i).equals("<")) {
				right = stack.pop();
				left = stack.pop();
				
				// Check if left is a key in user and if the right arg is an integer, do comparison, then push to stack
				if (user.containsKey(left) && isInteger(right)) {
					int num1 = Integer.parseInt(user.get(left));
					int num2 = Integer.parseInt(right);
					
					stack.push(Boolean.toString(num1 < num2));
				}
				else
					stack.push("false");
			}
			else if (output.get(i).equals(">=")) {
				right = stack.pop();
				left = stack.pop();
				
				// Check if left arg is a key in user and if the right arg is an integer, do comparison, then push to stack
				if (user.containsKey(left) && isInteger(right)) {
					int num1 = Integer.parseInt(user.get(left));
					int num2 = Integer.parseInt(right);
					
					stack.push(Boolean.toString(num1 >= num2));
				}
				else
					stack.push("false");
			}
			else if (output.get(i).equals("<=")) {
				right = stack.pop();
				left = stack.pop();
				
				// Check if left arg is a key in user and if the right arg is an integer, do comparison, then push to stack
				if (user.containsKey(left) && isInteger(right)) {
					int num1 = Integer.parseInt(user.get(left));
					int num2 = Integer.parseInt(right);
					
					stack.push(Boolean.toString(num1 <= num2));
				}
				else
					stack.push("false");
			}
			else if (output.get(i).equals("=")) {
				right = stack.pop();
				left = stack.pop();
				
				// If left arg exists, do direct comparison to right arg
				if(user.containsKey(left))
					stack.push(Boolean.toString(user.get(left).equals(right)));
				else
					stack.push("false");
			}
			else
				stack.push(output.get(i));
			
		}
		
		//System.out.println(stack);
		
		if (stack.isEmpty())
			return false;
		else 
			// Should be left with a single boolean on stack
			return Boolean.valueOf(stack.pop());
		
	}

	public void display() {
		System.out.println("Initial Expression: " + initialExpression);
		System.out.println("Post Fix: (TOP)" + output + " (BOTTOM)");
	}
	
	
	///// Private Methods /////
	private void createPostFixExpression(String infix) {
		
		Deque<String> operatorStack = new LinkedList<String>();
		
		for(String token : infix.split("\\s")) {
			
			// Operator precedence:
			// 6 ), ], }
			// 5 <, >, <=, >=, =, exists: evaluate immediate and output a true
			// 4 EXISTS
			// 3 NOT
			// 2 AND
			// 1 OR
			// 0 (, [, {

			// Logical Operator
			if (LogicalOperators.containsKey(token)) {
				while (!operatorStack.isEmpty() && isHigherPrecedence(token, operatorStack.peek()))
					output.push(operatorStack.pop());
				operatorStack.push(token);
			}
			// left parenthesis
			else if (token.equals("(")) {
				operatorStack.push(token);
			}
			// right parenthesis
			else if (token.equals(")")) {
				while(!operatorStack.peek().equals("("))
					output.push(operatorStack.pop());
				operatorStack.pop();
			}
			// Misc key or value
			else {	
				output.push(token);
			}
			
		}
		
		// Push rest of the operators onto the output stack
		while(!operatorStack.isEmpty())
			output.push(operatorStack.pop());
	}

	private boolean isHigherPrecedence(String op, String sub) {
		
		if (LogicalOperators.containsKey(sub) &&
			LogicalOperators.get(sub).precedence >= LogicalOperators.get(op).precedence)
			return true;
		return false;
	}
	
	private static boolean isInteger(String s) {
		return isInteger(s, 10);
	}
	
	private static boolean isInteger(String s, int radix) {
		if (s.isEmpty()) return false;
		for(int i = 0; i < s.length(); i++) {
			if(i == 0 && s.charAt(i) == '-') {
				if(s.length() == 1) return false; 
				else continue;
			}
			if(Character.digit(s.charAt(i), radix) < 0) return false;
		}
		return true;
	}
}
