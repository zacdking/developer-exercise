package com.forgerock;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

public class FilterTest {	
	
	private LinkedHashMap<String, String> initialize() {
		LinkedHashMap<String, String> user = new LinkedHashMap<String, String>();
		
		// Create user resource having various properties
		user.put("firstname", "Joe");
		user.put("surname", "Blogg");
		user.put("role", "administrator");
		user.put("age", "35");
				
		return user;
	}
	
	@Test
	public void test1() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("( role = administrator ) AND ( age > 30 )");
		
		assert filter.matches(user);
	}
	
	@Test
	public void test2() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("NOT ( role = developer ) OR NOT ( age > 3 )");
		
		assert filter.matches(user);
	}
	
	@Test
	public void test3() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("firstname = Beth OR ( surname = Blogg AND age = 35 )");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testEXISTS() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("EXISTS surname");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testNOT() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("NOT EXISTS lastname");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testAND() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("true AND true");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testOR() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("false OR true");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testGT() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("age > 30");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testLT() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("age < 40");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testGE() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("age >= 35");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testLE() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("age <= 35");
		
		assert filter.matches(user);
	}
	
	@Test
	public void testEQ() {
		
		LinkedHashMap<String, String> user = initialize();
		
		// Create a filter which matches all administrators older than 30
		Filter filter = new Filter("firstname = Joe");
		
		assert filter.matches(user);
	}
}
