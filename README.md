
## ForgeRock Developer Exercise
---
This is a Maven Project written in Java version 1.8.0_241. 

### Usage:

The filter package includes a filter class. This class can be used to distinguish "Users" based on their unique properties. "Users" in this case are just LinkedHashMaps that have a set of properties and corresponding values.

At the moment, the filter can interpret boolean literal (constants) "true" and "false". Logical operators such as "AND", "OR", and "NOT"; along with the comparison operators "=", ">", "<", ">=", and "<=". Checking if a property exists is also an option by using "EXISTS".

All recognizable actions:

* (predicate) AND (predicate)

* (predicate) OR (predicate)

* NOT (predicate)

* EXISTS (property name)

* (property name) = (value)

* (property name) > (numerical value)

* (property name) < (numerical value)

* (property name) >= (numerical value)

* (property name) <= (numerical value)


All these can be used together and put into a single expression. Parenthesis are also allowed. Here's an example of a filter which matches all administrators odler than 30:

`		Filter filter = new Filter("( role = administrator ) AND ( age > 30 )");`

Notice how all parentheses, properties, values, and operators are separated with spaces. This is a necessary for the filter to comprehend the expression properly.

### Notes:

* FilterText.java has many tests with more usage examples.

* The filter class parses strings (even though this wasn't recommended). 

* This program does not support regular expressions.

* There is minimal error checking for the expressions. Typing something in wrong could possibly break the program.

* Precedence will be accounted for using the [Shunting Yard Algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm). This takes an infix expression and converts it to postfix notation, which makes it easier to preserve precedence.

### Extensibility:

##### a. Consider how the API could be extended to include support for new types of filters.

 Adding more filters could be easy or difficult due to the way I set things up. New operators would be very easy to implement. You would just add the operator to the enum with the respective precedence then include the functionality in the matches method. Most of the operators that would be applicable (operators that result in true or false) would likely fall into a similar category of the existing operators and would have similar logic.  Adding something like regular expressions would be a little more complicated, but still manageable with some limitations. The strict delimiter of spaces would make it easy to pick up any string of characters as long as they don't include spaces, existing operator names', or any preexisting limitations to Java strings.



##### b. Consider how the API could support 3rd party applications which need to perform some logic based on the structure and content of a filter in a type-safe manner.

 The API could be packaged up and used quickly as long as the data type involved is strictly a LinkedHashMap. The practicality of this would need to increase substantially though for it to be viable. For instance, the feature to search multiple "Users" at the same time would be very helpful. If you have multiple "Users" in the first place, using modern databases like SQL would likely be more helpful in the long run. SQL's query language is obviously more sound and SQL in general is able to store non-volatile memory and access it fairly quick.